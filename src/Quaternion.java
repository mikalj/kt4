import java.util.Objects;

/** Quaternions. Basic operations. */
public class Quaternion {
   private static final double DELTA = 0.000001;
   private final double a;
   private final double b;
   private final double c;
   private final double d;
   // TODO!!! Your fields here!

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
      this.a = a;
      this.b = b;
      this.c = c;
      this.d = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return a;
   }

   /** Imaginary part i of the quaternion.
    * @return imaginary part i
    */
   public double getIpart() {
      return b;
   }

   /** Imaginary part j of the quaternion.
    * @return imaginary part j
    */
   public double getJpart() {
      return c;
   }

   /** Imaginary part k of the quaternion.
    * @return imaginary part k
    */
   public double getKpart() {
      return d;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion:
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
      StringBuilder str = new StringBuilder();
      if (Math.abs(a) > DELTA) {
         str.append(a);
      }
      if (Math.abs(b) > DELTA) {
         if (b > 0) {
            str.append("+");
         }
         str.append(b);
         str.append("i");
      }
      if (Math.abs(c) > DELTA) {
         if (c > 0) {
            str.append("+");
         }
         str.append(c);
         str.append("j");
      }
      if (Math.abs(d) > DELTA) {
         if (d > 0) {
            str.append("+");
         }
         str.append(d);
         str.append("k");
      }
      return String.valueOf(str);
      /*if (Math.abs(a) < DELTA) {
         a = 0;
      }
      if (Math.abs(b) < DELTA) {
         b = 0;
      }
      if (Math.abs(c) < DELTA) {
         c = 0;
      }
      if (Math.abs(d) < DELTA) {
         d = 0;
      }*/
      //return a + "+" + b + "i+" + c + "j+" + d + "k";
   }

   /** Conversion from the string to the quaternion.
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {
      Double[] doubles = new Double[4];
      String[] chars = s.trim().split("");
      StringBuilder lastPart = new StringBuilder();
      for (String character : chars) {
         if (character.equals("i") && doubles[1] == null) {
            doubles[1] = Double.parseDouble(lastPart.toString());
            lastPart.delete(0, lastPart.length()); // https://mkyong.com/java/how-to-clear-delete-the-content-of-stringbuffer/
            continue;
         }
         if (character.equals("j") && doubles[2] == null) {
            doubles[2] = Double.parseDouble(lastPart.toString());
            lastPart.delete(0, lastPart.length());
            continue;
         }
         if (character.equals("k") && doubles[3] == null) {
            doubles[3] = Double.parseDouble(lastPart.toString());
            lastPart.delete(0, lastPart.length());
            continue;
         }
         if ((character.equals("-") || character.equals("+")) && lastPart.length() != 0) {
            doubles[0] = Double.parseDouble(String.valueOf(lastPart));
            lastPart.delete(0, lastPart.length());
            if (character.equals("-")) {
               lastPart.append(character);
            }
            continue;
         }
         if (character.equals("-")) {
            lastPart.append(character);
            continue;
         }
         if (character.equals(".")) {
            lastPart.append(character);
            continue;
         }
         try {
            Double.parseDouble(character);
            lastPart.append(character);
         } catch (IllegalArgumentException e) {
            //System.out.println("String " + s + " is not proper quaternion.");
         }
         if (character.equals("i") || character.equals("j") || character.equals("k")) {
            throw new IllegalArgumentException("String " + s + " is not proper quaternion.");
         }
      }
      if (lastPart.length() > 0) {
         doubles[0] = Double.parseDouble(String.valueOf(lastPart));
      }
      double aDouble = 0;
      double bDouble = 0;
      double cDouble = 0;
      double dDouble = 0;
      if (doubles[0] != null) {
         aDouble = doubles[0];
      }
      if (doubles[1] != null) {
         bDouble = doubles[1];
      }
      if (doubles[2] != null) {
         cDouble = doubles[2];
      }
      if (doubles[3] != null) {
         dDouble = doubles[3];
      }
      /*String[] parts = s.trim().split("\\+");
      if (parts.length != 4) {
         throw new IllegalArgumentException("String " + s + " is not proper quaternion.");
      }
      double a = Double.parseDouble(parts[0]);
      double b = Double.parseDouble(parts[1].replace("i", ""));
      double c = Double.parseDouble(parts[2].replace("j", ""));
      double d = Double.parseDouble(parts[3].replace("k", ""));*/
      return new Quaternion(aDouble, bDouble, cDouble, dDouble);
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Quaternion(a, b, c, d);
   }

   /** Test whether the quaternion is zero.
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      return Math.abs(a) < DELTA && Math.abs(b) < DELTA && Math.abs(c) < DELTA && Math.abs(d) < DELTA;
   }

   /** Conjugate of the quaternion. Expressed by the formula
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
      return new Quaternion(a, b * -1, c * -1, d * -1);
   }

   /** Opposite of the quaternion. Expressed by the formula
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
      return new Quaternion(a * -1, b * -1, c * -1, d * -1);
   }

   /** Sum of quaternions. Expressed by the formula
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
      return new Quaternion(a + q.a, b + q.b, c + q.c, d + q.d);
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
      return new Quaternion((a*q.a - b*q.b - c*q.c - d*q.d), (a*q.b + b*q.a + c*q.d - d*q.c),
              (a*q.c - b*q.d + c*q.a + d*q.b), (a*q.d + b*q.c - c*q.b + d*q.a));
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
      return new Quaternion(a * r, b * r, c * r, d * r);
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
      if (this.isZero()) {
         throw new RuntimeException("Can't inverse zero.");
      }
      return new Quaternion(a/(a*a+b*b+c*c+d*d), (-b)/(a*a+b*b+c*c+d*d), (-c)/(a*a+b*b+c*c+d*d), (-d)/(a*a+b*b+c*c+d*d));
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
      return new Quaternion(a - q.a, b - q.b, c - q.c, d - q.d);
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
      if ((Math.abs(q.a) + Math.abs(q.b) + Math.abs(q.c) + Math.abs(q.d)) < DELTA){
         throw new RuntimeException("Error while inversing when trying to divide by right: Cannot divide by zero!");
      }
      return this.times(q.inverse());
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
      if ((Math.abs(q.a) + Math.abs(q.b) + Math.abs(q.c) + Math.abs(q.d)) < DELTA){
         throw new RuntimeException("Error while inversing when trying to divide by right: Cannot divide by zero!");
      }
      return q.inverse().times(this);
   }

   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
      return this.minus(Quaternion.valueOf(qo.toString())).isZero();
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
      return (this.times(q.conjugate()).plus(q.times(conjugate()))).times(0.5);
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(a, b, c, d);
   }

   /** Norm of the quaternion. Expressed by the formula
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(a*a + b*b + c*c + d*d);
   }

   /** Main method for testing purposes.
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: "
              + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));
      Quaternion arv3 = new Quaternion (-1., 0, 2., -2.);
      String arv3String = arv3.toString();
      System.out.println(arv3.toString());
      System.out.println(valueOf(arv3String));
   }
}
// end of file